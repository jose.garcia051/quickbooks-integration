var express = require('express');
var router = express.Router();
var Server = require('quickbooks-js');
var qbXMLHandler = require('../qbXMLHandler');

/* GET home page. */
router.get('/', function(req, res, next) {

  var soapServer = new Server();

  soapServer.setQBXMLHandler(qbXMLHandler);
  soapServer.run();
});

module.exports = router;
