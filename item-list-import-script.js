var arangojs = require("arangojs");

const db = new arangojs.Database({
    url: "http://35.184.123.88:8529"
});

db.useDatabase("Specbooks");

const itemList = require(process.argv[3]);

const up = async () => {
    const done = await db.collection('qb_customer_item_list').save({
        list: itemList,
        _key: process.argv[2]
    });

    console.log(done);
};

up();
