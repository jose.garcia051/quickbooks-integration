
var Server = require('./quickbooks-js/index');
var qbXMLHandler = require('./qbXMLHandler');
require('dotenv').config();
var soapServer = new Server();

// SOAP Server
soapServer.setQBXMLHandler(qbXMLHandler);
soapServer.run();

