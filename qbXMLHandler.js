/*
 * This file is part of quickbooks-js
 * https://github.com/RappidDevelopment/quickbooks-js
 *
 * Based on qbws: https://github.com/johnballantyne/qbws
 *
 * (c) 2015 johnballantyne
 * (c) 2016 Rappid Development LLC
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

var data2xml = require('data2xml');
var arangojs = require('arangojs');
const axios = require('axios');


const db = new arangojs.Database({
    url: "http://35.184.123.88:8529"
});

db.useDatabase("Specbooks");



var convert = data2xml({
    xmlHeader: '<?xml version="1.0" encoding="utf-8"?><?qbxml version="13.0"?>'
});

/**
 *
 * @param companyName: string
 * @param callback: function
 * @returns {Promise<*>}
 */
async function buildRequests(companyName, callback) {
    const requests = [];
    const name = 'sample';
    const key = 'fK4/wY7seAo#->GX|;J4cmFUer0|}6?7":,Q*TJklem.%<AP4~=9Xgg(jaLw+Xj';
    console.log(companyName);
    let sbDetails = await axios.get(`https://specbooks.com/api/company/${companyName}/specbooks/status/approved?key=${encodeURIComponent(key)}`);
    let itemListDoc = null;

    if (companyName.indexOf('santafebydesign') > -1 || companyName.indexOf('specbooks') > -1) {

        try {
            itemListDoc = await db.collection('qb_customer_item_list').document('santafebydesign').catch(err => {
                console.log(err);
                itemListDoc = null;
            });
        } catch (e) {
            console.log(e);
            itemListDoc = null;
        }
    }


    const itemList = itemListDoc ? itemListDoc.list : [];

    for (let i in sbDetails.data.data.items) {
        if (!sbDetails.data.data.items[i]) {
            continue;
        }

        const sb = sbDetails.data.data.items[i];

        if (!sb.customer_number || sb.customer_number === '') {
            continue;
        }


        const acctObj = {
            QBXMLMsgsRq: {
                _attr: {
                    onError: 'continueOnError'
                },
                AccountAddRq: {
                    AccountAdd: {
                        Name: 'SpecBooks Income',
                        AccountType: 'Income'
                    }
                }
            }
        };

        var acctXml = convert(
            'QBXML',
            acctObj);

        requests.push(acctXml);

        const secondAccountObj = {
            QBXMLMsgsRq: {
                _attr: {
                    onError: 'stopOnError'
                },
                AccountAddRq: {
                    AccountAdd: {
                        Name: 'SpecBooks Asset',
                        AccountType: 'OtherAsset'
                    }
                }
            }
        };


        var secondAcctXml = convert(
            'QBXML',
            secondAccountObj
        );

        requests.push(secondAcctXml);


        const cogsAccountObj = {
            QBXMLMsgsRq: {
                _attr: {
                    onError: 'continueOnError'
                },
                AccountAddRq: {
                    AccountAdd: {
                        Name: 'SpecBooks COGS',
                        AccountType: 'CostOfGoodsSold'
                    }
                }
            }
        };


        var cogsAcctXml = convert(
            'QBXML',
            cogsAccountObj
        );

        requests.push(cogsAcctXml);

        const cusObj = {
            QBXMLMsgsRq: {
                _attr: {
                    onError: 'continueOnError'
                },
                CustomerAddRq: {
                    CustomerAdd: {
                        Name: sb.customer_number.replace(/\u0026/g, " ").replace(/\n/g, ' ').replace(/\u0022/g,' ').replace(/\u[a-zA-Z0-9] /g, ' ')
                    }
                }
            }
        };


        var cusXml = convert(
            'QBXML',
            cusObj);

        requests.push(cusXml);


        const xmlObj = {
            QBXMLMsgsRq : {
                _attr: {
                    onError: 'continueOnError'
                },
                EstimateAddRq: {
                    EstimateAdd: {
                        CustomerRef: {
                            FullName: sb.customer_number.replace(/\u0026/g, " ").replace(/\n/g, ' ').replace(/\u0022/g,' ').replace(/\u[a-zA-Z0-9] /g, ' ')
                        },
                        EstimateLineAdd: []
                    }
                }
            }
        };

        for (let ci in sb.categories) {
            if (!sb.categories[ci]) {
                continue;
            }

            const c = sb.categories[ci];


            const catObj = {
                QBXMLMsgsRq: {
                    _attr: {
                        'onError': 'continueOnError'
                    },
                    ItemInventoryAddRq: {
                        ItemInventoryAdd: {
                            Name: 'CAT',
                            IncomeAccountRef: {
                                FullName: 'SpecBooks Income'
                            },
                            COGSAccountRef: {
                                FullName: 'SpecBooks COGS'
                            },
                            AssetAccountRef: {
                                FullName: 'SpecBooks Asset'
                            }
                        }
                    }
                }
            };

            var catXml = convert(
                'QBXML',
                catObj);

            requests.push(catXml);

            xmlObj.QBXMLMsgsRq.EstimateAddRq.EstimateAdd.EstimateLineAdd.push({
                ItemRef: {
                    FullName: 'CAT'
                },
                Desc: c.name,
                Quantity: 1
            });

            for (let si in c.specs) {
                if (!c.specs[si]) {
                    continue;
                }

                const s = c.specs[si];
                let qbItem = s.model_number;
                let isExisting = false;

                for (let i = 0; i < itemList.length; i++) {
                    const item = itemList[i];

                    if (item.toLowerCase().indexOf(qbItem.toLowerCase()) > -1) {
                        qbItem = item.indexOf('=:') > -1 ? item.split('=:')[1] : item;
                        isExisting = true;

                        break;
                    }
                }


                 if (!isExisting) {
                    const itemObj = {
                        QBXMLMsgsRq: {
                            _attr: {
                                'onError': 'continueOnError'
                            },
                            ItemInventoryAddRq: {
                                ItemInventoryAdd: {
                                    Name: s.brand + ' ' + s.model_number,
                                    ManufacturerPartNumber: s.brand + ' ' + s.model_number,
                                    SalesPrice: s.price,
                                    IncomeAccountRef: {
                                        FullName: 'SpecBooks Income'
                                    },
                                    COGSAccountRef: {
                                        FullName: 'SpecBooks COGS'
                                    },
                                    AssetAccountRef: {
                                        FullName: 'SpecBooks Asset'
                                    },
                                }
                            }
                        }
                    };
                    var itemXml = convert(
                        'QBXML',
                        itemObj);

                    requests.push(itemXml);
                }



                qbItem = qbItem.replace(/\u0026/g, " ").replace(/\n/g, ' ').replace(/\u0022/g,' ').replace(/\u[a-zA-Z0-9] /g, ' ');
                 //
                const desc = (s.brand + ' ' + s.model_number + ': ' + s.name).replace(/\u0026/g, " ").replace(/\n/g, ' ').replace(/\u0022/g,' ').replace(/\u[a-zA-Z0-9] /g, ' ').replace(/\u00ae/g, ' ').replace(/®/g, ' ');
                console.log(qbItem, desc);

                xmlObj.QBXMLMsgsRq.EstimateAddRq.EstimateAdd.EstimateLineAdd.push({
                    ItemRef: {
                      FullName: s.brand + ' ' + s.model_number
                    },
                    Desc: desc,
                    Quantity: s.quantity,
                    Rate: s.sale_price
                });
            }
        }

        var xml = convert(
            'QBXML',
            xmlObj);

        requests.push(xml);

        axios.post(`https://specbooks.com/api/specbook/${sb.specbookID}/status/submitted`, {
            key: key,
            value: 'submitted'
        }).then(res => {
            }).catch(err => {
                console.log(err);
        });
    }

    return callback(null, requests);
}

// Public
module.exports = {

    /**
     * Builds an array of qbXML commands
     * to be run by QBWC.
     *
     * @param callback(err, requestArray)
     */
    fetchRequests: function(userName, callback) {
        buildRequests(userName, callback);
    },

    /**
     * Called when a qbXML response
     * is returned from QBWC.
     *
     * @param response - qbXML response
     */
    handleResponse: function(response) {
        console.log(response);
    },

    /**
     * Called when there is an error
     * returned processing qbXML from QBWC.
     *
     * @param error - qbXML error response
     */
    didReceiveError: function(error) {
        console.log(error);
    }
};
